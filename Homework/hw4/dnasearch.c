/*  
    Homework 4, 600.120 Spring 2017

    Angelica Walker
    Section 02 - Kazhdan
    03/08/17
    awalke57
    awalke57@jhu.edu

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

int check(char text[]) {

  int t = 0;
  
  for(unsigned int j = 0; j < strlen(text); j++) {

    char checking = text[j];

    switch(checking) {
    case 'G': case 'A': case 'C': case 'T': case 'g': case 'a': case 'c': case 't':
      t = 1;
      break;
    default:
      t = 0;
      break;
    }
      
  }

  return t;
}

int *search(char text[], char pattern[], int charNum) {
  
  int lenPattern = strlen(pattern);
  int *occ = malloc(sizeof(int) * charNum);
  int nums = 0;
  
  for (int i = 0; i <= charNum - lenPattern; i++) {
    
    bool match = true;
    
    for (int k = 0; k < lenPattern; k++) {

      char patternChar = pattern[k];
      char textChar = text[i + k];

      if (patternChar != textChar) {
	match = false;
        break;
      }

      if (match && k == lenPattern - 1) {
	occ[nums] = i;
	nums++;
      }

    }

  }
  
  for (int h = nums; h < charNum; h++) {

    occ[h] = -1;
  }

  return occ;

}
