/*
    Homework 4, 600.120 Spring 2017

    Angelica Walker
    Section 02 - Kazhdan
    03/08/17
    awalke57
    awalke57@jhu.edu

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "dnasearch.h"
#include <ctype.h>

int main(int argc, char *argv[]) {

  FILE* fp = fopen(argv[1], "r");
  char text[15000] = {0};
  int charNum = 0;

  if (argc == 1) {
    return 1;
  }
  
  if (!fp) {
    printf("Error opening text file");
    return 1;
  }

  int i = 0;
  while(!feof(fp) && i < 15000) {

      char c = fgetc(fp);
      if (!isspace(c)) {
	text[charNum] = c;
	charNum++;
      }
      i++;

  }

  text[charNum - 1] = '\0';

  fclose(fp);

  //printf("%s", text);

  int testing = check(text);

  if (testing == 0) {
    printf("Invalid pattern");
    return 1;
  }
  
  char pattern[15000];
  int *found;
  
  while (scanf("%s", pattern) != EOF) {

    int checking = check(pattern);

    if (checking == 1) {
      found = search(text, pattern, charNum);
      printf("%s ", pattern);
      
      for (int j = 0; j < charNum; j++) {

	if (found[j] != -1) {
	  printf("%d ", found[j]);
	}
	else if (found[0] == -1) {
	  printf("Not found");
	  break;
	}

      }

      printf("\n");
    }

    if (checking == 0) {
      printf("Invalid pattern");
      return 1;
    }
    
  }

  free(found);
  return 0;
}
