/*
    Homework 4, 600.120 Spring 2017

    Angelica Walker
    Section 02 - Kazhdan
    03/08/17
    awalke57
    awalke57@jhu.edu

*/

#include "dnasearch.h"
#include <stdio.h>

int main() {

  // Testing Check
  int value;
  
  value = check("A");
  if (value != 1) {
    printf("Failed");
  }
  
  value = check("a");
  if (value != 1) {
    printf("Failed");
  }

  value = check("G");
  if (value != 1) {
    printf("Failed");
  }

  value = check("g");
  if (value != 1) {
    printf("Failed");
  }

  value = check("T");
  if (value != 1) {
    printf("Failed");
  }

  value = check("t");
  if (value != 1) {
    printf("Failed");
  }

  value = check("C");
  if (value != 1) {
    printf("Failed");
  }

  value = check("c");
  if (value != 1) {
    printf("Failed");
  }

  value = check("CAT");
  if (value != 1) {
    printf("Failed");
  }

  value = checl("AND");
  if (value != 0) {
    printf("Failed");
  }

  // Testing Search
  int *occ;
  
  occ = search("CATATGCTGATGCT", "CAT", 14);
  
  if (occ[0] != 0) {
    printf("Failed test 1");
  }

  occ = search("GTAGTGATG", "TAA", 9);

  if (occ[0] != -1) {
    printf("Failed test 2");
  }

  occ = search("TAGATCGATGCTA", "TA", 13);

  if (occ != 0 && occ[1] != 11) {
    printf("Failed test 3");
  }
  
  return 0;
}
