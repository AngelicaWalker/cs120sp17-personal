/*  File: hw1.c
    Homework 1, 600.120 Spring 2017

    Angelica Walker
    Section 02 - Kazhdan
    2/13/17
    awalke57
    awalke57@jhu.edu

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define LENGTH 16  //LENGTH is now a constant with value 16

int main(int argc, char* argv[]) {

    // Confirm that a command-line argument is present
    if (argc == 1) {
        printf("Usage: hw0 XX.###.###Gg#.#\n");
        return 1;  // exit program because no command line argument present
    }

    // Declare a char array to hold the command-line argument string; 
    // ensure last char is null character
    char course[LENGTH];
    strncpy(course, argv[1], LENGTH);
    course[LENGTH-1] = '\0';

    // TO DO: eventually remove the line below; it's just for debugging purposes 
    // printf("Course string collected: %s\n", course);


    // TO DO: add your code here


    ////////////////////////////////////////////////////////////////////////////
    // CODE FOR DIVISION ///////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    char division[] = "";
    int divisionCode = 0;
    
    division[0] = course[0];
    division[1] = course[1];
    division[2] = '\0';


    if (strcmp(division, "ME") == 0) {
      divisionCode = 0;
    }
    if (strcmp(division, "BU") == 0) {
      divisionCode = 1;
    }
    if (strcmp(division, "ED") == 0) {
      divisionCode = 2;
    }
    if (strcmp(division, "EN") == 0) {
      divisionCode = 3;
    }
    if (strcmp(division, "AS") == 0) {
      divisionCode = 4;
    }
    if (strcmp(division, "PH") == 0) {
      divisionCode = 5;
    }
    if (strcmp(division, "PY") == 0) {
      divisionCode = 6;
    }
    if (strcmp(division, "SA") == 0) {
      divisionCode = 7;
    }
    
    printf("Division: %d\n", divisionCode); 

    ////////////////////////////////////////////////////////////////////////////
    // CODE FOR DEPARTMENT /////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////  
    
    char departmentCode[] = "";

    departmentCode[0] = course[3];
    departmentCode[1] = course[4];
    departmentCode[2] = course[5];
    departmentCode[3] = '\0';

    int conversion1 = atoi(departmentCode);

    printf("Department: %d\n", conversion1);
    
    ////////////////////////////////////////////////////////////////////////////                   
    // CODE FOR COURSE CODE ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    
    char courseCode[] = "";

    courseCode[0] = course[7];
    courseCode[1] = course[8];
    courseCode[2] = course[9];
    courseCode[3] = '\0';

    int conversion2 = atoi(courseCode);

    printf("Course: %d\n", conversion2);

    ////////////////////////////////////////////////////////////////////////////
    // CODE FOR GRADE //////////////////////////////////////////////////////////  
    ////////////////////////////////////////////////////////////////////////////  
    
    char grade = course[10];
    char gradeSymbol = course[11];
    int gradeNum = 0;
    int gradeSymbolNum = 0;

    switch (grade) {
    case 'A':
      gradeNum = 0;
      break;
    case 'B':
      gradeNum = 1;
      break;
    case 'C':
      gradeNum = 2;
      break;
    case 'D':
      gradeNum = 3;
      break;
    case 'F':
      gradeNum = 4;
      break;
    case 'I':
      gradeNum = 5;
      break;
    case 'S':
      gradeNum = 6;
      break;
    case 'U':
      gradeNum = 7;
      break;
    }

    switch (gradeSymbol) {

    case '+':
      gradeSymbolNum = 0;
      break;
    case '-':
      gradeSymbolNum = 1;
      break;
    case '/':
      gradeSymbolNum = 2;
      break;
    }
     
    printf("Grade: %d %d\n", gradeNum, gradeSymbolNum);

    ////////////////////////////////////////////////////////////////////////////                  
    // CODE FOR NUMBER OF CREDITS //////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////  

    char credits[] = "";

    credits[0] = course[12];
    credits[1] = '\0';

    int conversion3 = atoi(credits);


    
    char halfCredit[] = "";
    int halfCreditNum = 0;

    halfCredit[0] = course[14];
    halfCredit[1] = '\0';

    int conversion4 = atoi(halfCredit);

    if (conversion4 == 0) {
      halfCreditNum = 0;
    }
    if (conversion4 == 5) {
      halfCreditNum = 1;
    }    
    
    printf("Credits: %d %d\n", conversion3, halfCreditNum);
    
  return 0;
}
