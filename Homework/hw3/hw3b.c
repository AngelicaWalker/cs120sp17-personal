/*  File: hw3b.c
    Homework 3, 600.120 Spring 2017

    Angelica Walker
    Section 02 - Kazhdan
    2/28/17
    awalke57
    awalke57@jhu.edu

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void printMenu() {

  printf(
        "n - display the total number of courses\n"
        "d - list all courses from a particular department\n"
        "l - list all courses with a particular letter grade\n"
        "c - list all courses with at least a specified number of credits\n"
        "g - compute the GPA of all the courses with letter grades\n"
        "q - quit the program\n"
        "Enter letter choice ->\n");
}

void promptN(int i) {
    printf("Total number of courses: %d\n", i); 
}

void promptD() {
  int departmentNum;
  printf("Enter the department:\n");
  scanf("%d", departmentNum);
}

float promptL() {
  char grade[2];
  printf("Enter a letter grade and mark (+,-,/):\n");
  scanf("%s", grade);
  float gpa = 0;
  //printf("%c %c", letter, sign);

  if (strcmp(grade, "A+") == 0) {
    gpa = 4.0;
  }
  if (strcmp(grade, "A/") == 0) {
    gpa	= 4.0;
  }
  if (strcmp(grade, "A-") == 0) {
    gpa	= 3.7;
  }
  if (strcmp(grade, "B+") == 0) {
    gpa	= 3.3;
  }
  if (strcmp(grade, "B/") == 0) {
    gpa = 3.0;
  }
  if (strcmp(grade, "B-") == 0) {
    gpa = 2.7;
  }
  if (strcmp(grade, "C+") == 0) {
    gpa	= 3.3;
  }
  if (strcmp(grade, "C/") == 0) {
    gpa = 2.0;
  }
  if (strcmp(grade, "C-") == 0) {
    gpa = 1.7;
  }
  if (strcmp(grade, "D+") == 0) {
    gpa	= 1.3;
  }
  if (strcmp(grade, "D/") == 0) {
    gpa = 1.0;
  }

  return gpa;
}

int promptC() {
  int numocred;
  printf("Enter number of credits:\n");
  scaf("%d", numocred);
  return int;
}

void promptG(float gpa, int numofcredits) {
  int totalGPA;
  totalGPA = (numofcredits * gpa) / numofcredits;
  printf("GPA: %.3f\n", totalGPA);
}

void noMatches() {
    printf("No matches\n");
}

int main() {

  //printf("debugging");
  unsigned int inputs[2500];

  FILE* fp = fopen("courseInts.txt", "r");

  if (!fp) {
    printf("Error opening text file");
    return 1;
  }

  int i = 0;
  int num;
  int lineNums = 0;
  while(fscanf(fp, "%u", &num) > 0) {
    inputs[i] = num;
    i++;
    //printf("%d\n", num);
    lineNums++;
  }
  fclose(fp);

  //////////////////////////////////////////////////////////////////////////////
  //////PROGRAM TO TURN DECIMAL INTO READABLE INTS /////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  
  unsigned int temp[2500];
  unsigned int department[2500];
  unsigned int gradeNum[2500];
  unsigned int gradeSymbol[2500];
  unsigned int credits[2500];
  unsigned int halfCredits[2500];

  
  for (int j = 0; j < 2500; j++) {
    if (inputs[j] != 0) {
      inputs[j] = temp[j];
      halfCredits[j] = temp[j] % 2;
      //printf("%u", halfCredits[j]);
      credits[j] = (temp[j] / 2) % 8;
      //printf("%d", credits[j]);
      gradeSymbol[j] = (temp[j] / 16) % 4;
      //printf("%d", gradeSymbol[j]);
      gradeNum[j] = (temp[j] / 64) % 8;
      //printf("%d", gradeNum[j]);
      department[j] = (temp[j] / 524288) % 1024;
      //printf("%d", department[j]);
    }
  }
  
  
  char letterChoice;
  float GPA = 0;
  int numberOfCreds = 0;

  do {
    printMenu();
    //scanf("%c ", letterChoice);
    while ((letterChoice = getchar()) == '\n');
    
    switch (letterChoice) {
    case 'n': case 'N':
      promptN(lineNums);
      break;
    case 'd': case 'D':
      promptD();
      break;
    case 'l': case 'L':
      GPA = promptL();
      break;
    case 'c': case 'C':
      numberOfCreds = promptC();
      break;
    case 'g': case 'G':
      promptG(GPA, numberOfCreds);
      break;
    case 'q': case 'Q':
      break;
    default:
      noMatches();
      break;
    }
  } while (letterChoice != 'q');

  return 0;
}
