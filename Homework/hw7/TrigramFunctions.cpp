/*
    Homework 7, 600.120 Spring 2017

    Angelica Walker
    Section 02 - Kazhdan
    04/18/17
    awalke57
    awalke57@jhu.edu

*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <string>
#include <utility>
#include <algorithm>
#include <iterator>
#include <cstdlib>
#include <ctime>
#include <cstdio>
using std::cin;
using std::cout;
using std::vector;
using std::string;
using std::map;
using std::endl;
using std::ifstream;
using std::pair;

bool valid(string filename) {
  ifstream test(filename);
  bool check = test.good();
  test.close();
  return check;
}

void forward(map<vector<string>, int> * mapOfTrigrams) {

  for (map<vector<string>,int>::iterator i = mapOfTrigrams->begin(); i != mapOfTrigrams->end(); i++) {
    for(int j = 0; j < 3; j++) {
      cout << i->first[j] << ' ';
    }
    cout << i->second << endl;
  }

}
void reverse(map<vector<string>, int> * mapOfTrigrams) {

  for (map<vector<string>,int>::reverse_iterator i = mapOfTrigrams->rbegin(); i != mapOfTrigrams->rend(); i++) {
    for(int j = 0; j < 3; j++) {
      cout << i->first[j] << ' ';
    }
    cout << i->second << endl;
  }
}

void countOrder(map<vector<string>, int> * mapOfTrigrams) {

  int value = 1;
  int run = mapOfTrigrams->size();

  map<vector<string>, int>::iterator i;

  while(run) {
    for(i = mapOfTrigrams->begin(); i!= mapOfTrigrams->end(); i++) {
      if(i->second == value) {
        for(int j = 0; j < 3; j++) {
          cout << i->first[j] << ' ';
        }
        cout << i->second << endl;
        run--;
      }
    }
    value++;
  }
}
