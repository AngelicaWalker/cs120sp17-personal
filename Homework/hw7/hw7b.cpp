/*
    Homework 7, 600.120 Spring 2017  

    Angelica Walker
    Section 02 - Kazhdan
    04/18/17
    awalke57
    awalke57@jhu.edu

*/
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <string>
#include <utility>
#include <algorithm>
#include <iterator>
#include <cstdlib>
#include <ctime>
#include <cstdio>
using std::cin;
using std::cout;
using std::vector;
using std::string;
using std::map;
using std::endl;
using std::ifstream;
using std::pair;

void print(map<vector<string>, map<string, int>> mCounts);
string randomGen(map<string, int> &choiceMap);

int main(int argc, char* argv[]){
  srand(time(NULL));

  map<vector<string>, map<string, int>> output;

  if(argc != 3){
    cout << "Invalid number of arguments, program terminated" <<endl;
    return 0;
  }

  string argv1 = argv[1];

  ifstream input(argv1);
  vector<string> model;
  string sentence12, sentence3;
  int buffer = 0;

  while(input >> sentence12) {
    if(buffer == 0 || buffer == 1) { //grabs the "context" words (word1, word2)
      model.push_back(sentence12);
      buffer++;
    }
    else if(buffer == 2) { //grabs the possiblity word (word3)
      sentence3 = sentence12;
      buffer++;
    }
    else {
      //adds the new possibilty and its frequency into map
      output[model].insert(pair<string, int> (sentence3, stoi(sentence12)));
      buffer = 0;
      //prepare for now trigram and counts
      model.clear();
      sentence3.clear();
    }
  }

  input.close();

  for(int i = 0; i < *argv[2]; i++) {
    print(output);
  }
  
  return 0;
}

void print(map<vector<string>, map<string, int>> output) {
    vector<string> printing;
    map<string, int> mapp;
    string temp;
    printing.push_back("<START_1>");
    printing.push_back("<START_2>");
    cout << printing[0] << ' ' << printing[1];

    while(printing[1] != "<END_1>") {

        mapp = (output.find(printing)->second);

        printing.erase(printing.begin());
        temp = randomGen(mapp);

        cout << ' ' << temp;

        printing.push_back(temp);

    }
    cout << ' ' << "<END_2>" << endl;
}

string randomGen(map<string, int> &choiceMap) {
    vector<string> vec;
    map<string, int>::iterator i;
    int count;

    for(i = choiceMap.begin(); i != choiceMap.end(); i++) {
        count = i->second;

        for(int j = 0; j < count; j++) {
            vec.push_back(i->first);
        }
    }

    count = vec.size();

    return vec[rand() % count];
}
