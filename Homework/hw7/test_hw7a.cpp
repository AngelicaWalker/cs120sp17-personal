/*                                                                     
    Homework 7, 600.120 Spring 2017                                    
                                                                       
    Angelica Walker                                                    
    Section 02 - Kazhdan                                               
    04/18/17                                                           
    awalke57                                                           
    awalke57@jhu.edu                                                   
                                                                       
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <string>
#include <utility>
#include <algorithm>
#include <iterator>
#include <cstdlib>
#include <ctime>
#include <cstdio>
#include <cassert>
#include "TrigramFunctions.hpp"
using std::cin;
using std::cout;
using std::vector;
using std::string;
using std::map;
using std::endl;
using std::ifstream;
using std::pair;

int main () {
    
    assert(valid("sl.txt") == true);
    assert(valid("incorrect") == false);

    return 0;

}
