/*
    Homework 6, 600.120 Spring 2017

    Angelica Walker
    Section 02 - Kazhdan
    04/18/17
    awalke57
    awalke57@jhu.edu

*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <string>
#include <utility>
#include <algorithm>
#include <iterator>
#include <cstdlib>
#include <ctime>
#include <cstdio>
using std::cin;
using std::cout;
using std::vector;
using std::string;
using std::map;
using std::endl;
using std::ifstream;
using std::pair;

bool valid(string filename);
void forward(map<vector<string>, int> * mapOfTrigrams);
void reverse(map<vector<string>, int> * mapOfTrigrams);
void countOrder(map<vector<string>, int> * mapOfTrigrams);

