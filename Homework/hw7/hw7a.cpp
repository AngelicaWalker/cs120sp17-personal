/*
    Homework 7, 600.120 Spring 2017

    Angelica Walker
    Section 02 - Kazhdan
    04/18/17
    awalke57
    awalke57@jhu.edu

*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <string>
#include <utility>
#include <algorithm>
#include <iterator>
#include <cstdlib>
#include <ctime>
#include <cstdio>
#include "TrigramFunctions.hpp"
using std::cin;
using std::cout;
using std::vector;
using std::string;
using std::map;
using std::endl;
using std::ifstream;
using std::pair;

int main(int argc, char* argv[]) {

  vector<string> * output = new vector<string>();;
  map<vector<string>, int> * mapOfTrigrams = new map<vector<string>, int>();;
  
  if(argc != 3) {
    cout << "Incorrect number of arguements, program terminated." << endl;
    return 0;
  }

  if(*argv[2] != 'a' && *argv[2] != 'r' && *argv[2] != 'c') {
    cout << "Incorrect arguements, program terminated." << endl;
    return 0;
  }
  
  string arg1 = argv[1];
  ifstream fp;
  fp.open(arg1);
  string inFile;
  while (fp >> inFile) {
    if(valid(inFile)) {
      output->push_back(inFile);
    }
  }
  fp.close();

  ifstream stream;
  string str;
  vector<string> vec;
  int count;

  for(vector<string>::iterator i = output->begin(); i != output->end(); i++) {
    stream.open(*i);
    vec.push_back("<START_1>");
    vec.push_back("<START_2>");
    while(stream >> str) {
      vec.push_back(str);

      if(mapOfTrigrams->find(vec) == mapOfTrigrams->end()) {
        count = 1;
        mapOfTrigrams->insert(pair<vector<string>, int>(vec, count));
      }
      else {
	mapOfTrigrams->at(vec)++;
      }
      

      vec.erase(vec.begin());
    }
    vec.push_back("<END_1>");

    
    if(mapOfTrigrams->find(vec) == mapOfTrigrams->end()) {
      count = 1;
      mapOfTrigrams->insert(pair<vector<string>, int>(vec, count));
    }
    else {
      mapOfTrigrams->at(vec)++;
    }
    
    vec.erase(vec.begin());
    vec.push_back("<END_2>");

    if(mapOfTrigrams->find(vec) == mapOfTrigrams->end()) {
      count = 1;
      mapOfTrigrams->insert(pair<vector<string>, int>(vec, count));
    }
    else {
      mapOfTrigrams->at(vec)++;
    }

    vec.clear();
    stream.close();
  }

  output->erase(output->end());
  output->erase(output->end());
  
  if(*argv[2] == 'a') {
    forward(mapOfTrigrams);
  }
  if(*argv[2] == 'r') {
    reverse(mapOfTrigrams);
  }
  if (*argv[2] == 'c') {
    countOrder(mapOfTrigrams);
  }
  
  for(vector<string>::iterator i = output->begin(); i != output->end(); i++) {
    cout << *i << ' ';
  }

  cout << endl;

  return 0;
}
