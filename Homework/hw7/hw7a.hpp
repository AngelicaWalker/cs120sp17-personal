/*
    Homework 7, 600.120 Spring 2017                                    
                                                                       
    Angelica Walker                                                    
    Section 02 - Kazhdan                                               
    04/18/17                                                           
    awalke57                                                           
    awalke57@jhu.edu   

*/

bool valid(string filename);
void forward(map<vector<string>, int> * mapOfTrigrams);
void reverse(map<vector<string>, int> * mapOfTrigrams);
void countOrder(map<vector<string>, int> * mapOfTrigrams);
