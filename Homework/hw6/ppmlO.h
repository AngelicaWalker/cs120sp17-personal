/*
    Homework 6, 600.120 Spring 2017

    Angelica Walker
    Section 02 - Kazhdan
    04/09/17
    awalke57
    awalke57@jhu.edu

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct image {

  unsigned char red;
  unsigned char green;
  unsigned char blue;
  
} Image;

int check(FILE *fp, int* height, int* width);
int read(char file[], Image ** image, int * height, int * width);
int write(char file[], Image ** image, int * height, int * width);
