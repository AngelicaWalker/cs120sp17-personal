/*
    Homework 6, 600.120 Spring 2017

    Angelica Walker
    Section 02 - Kazhdan
    04/09/17
    awalke57
    awalke57@jhu.edu

*/

int crop(Image ** image, int * height, int * width, int x1, int y1, int x2, int y2);
int invert(Image ** image, int * height, int * width);
int swap(Image ** image, int * height, int * width);
int grayscale(Image ** image, int * height, int * width);
int brightness(Image ** image, int * height, int * width, double brightening);
