/*
    Homework 6, 600.120 Spring 2017

    Angelica Walker
    Section 02 - Kazhdan
    04/09/17
    awalke57
    awalke57@jhu.edu

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ppmlO.h"

int crop(Image ** image, int * height, int * width, int x1, int y1, int x2, int y2) {

  if (x1 < 0 || x2 < 0 || x1 > (* width) || x2 > (* width)) {
    printf("Error: cropping failed, image unchanged\n");
    return 0;
  }
  
  if (y1 < 0 || y2 < 0 || y1 > (* height) || y2 > (* height)) {
    printf("Error: cropping failed, image unchanged\n");
    return 0;
  }

  int diffX = x1 - x2; 

  if (diffX < 0) {
    diffX *= -1;
  }
  
  else {
    x1 = x2;
    x2 += diffX;
  }
  
  int diffY = y1 - y2;

  if (diffY < 0) {
    diffY *= -1;
  }
  
  else {
    y1 = y2;
    y2 += diffY;
  }
  
  int newArea = diffX * diffY;
  Image * temp = malloc(sizeof(Image) * newArea);
  
  int k = 0;
  
  for(int i = y1; i < y2; i++){

    for(int j = x1; j < x2; j++){

      temp[k] = (* image)[(i) * ((* width)) + j];
      k++;

    }
  }

  * height = diffY;
  * width = diffX;
  * image = temp;

  return 0;
}

int invert(Image ** image, int * height, int * width) {

  for (int i = 0; i < (* height) * (* width); i++) {
      (* image)[i].red = 255 - (* image)[i].red;
      (* image)[i].blue = 255 - (* image)[i].blue;
      (* image)[i].green = 255 - (* image)[i].green;
   }
  
  return 0;
}

int swap(Image ** image, int * height, int * width) {

  int temp;
  
  for (int i = 0; i < (* width) * (* height); i++) {
	temp = (* image)[i].red;
	(* image)[i].red = (* image)[i].green;
	(* image)[i].green = (* image)[i].blue;
	(* image)[i].blue = temp;
  }

  return 0;
}

int grayscale(Image ** image, int * height, int * width) {

  for(int i = 0; i < (*width) * (*height); i++) {

    double red = .30 * (* image)[i].red;
    double green = .59 * (* image)[i].green;
    double blue = .11 * (* image)[i].blue;
    int intensity = red + green + blue;

    (* image)[i].red = intensity;
    (* image)[i].green = intensity;
    (* image)[i].blue = intensity;
  }

  return 0;
}

int brightness(Image ** image, int * height, int * width, double brightening) {

  for (int i = 0; i < (* width) * (* height); i++) {

    if ((* image)[i].red * brightening > 255){
      (* image)[i].red = 255;
    }
    else if ((*image)[i].red * brightening < 0) {
      (* image)[i].red = 0;
    }
    else {
      (* image)[i].red *= brightening;
    }


    if ((* image)[i].green * brightening > 255) {
      (* image)[i].green = 255;
    }
    else if ((* image)[i].green * brightening < 0) {
      (* image)[i].green = 0;
    }
    else {
      (* image)[i].green *= brightening;
    }

    
    if(( *image)[i].blue * brightening > 255) {
      (* image)[i].blue = 255;
    }
    else if((* image)[i].blue * brightening < 0) {
      (* image)[i].blue = 0;
    }
    else{
      (* image)[i].blue *= brightening;
    }
    
  }
  
  return 0;
}
