/*
    Homework 6, 600.120 Spring 2017

    Angelica Walker
    Section 02 - Kazhdan
    04/09/17
    awalke57
    awalke57@jhu.edu

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "menuUtil.h"

int main() {

  Image * image; 
  int * height = malloc(sizeof(int));
  int * width = malloc(sizeof(int));
  int * readCheck = malloc(sizeof(int));
  int * changed = malloc(sizeof(int));
  * changed = 0;

  char inputs[200];
  
  do{

    memset(inputs, 0, sizeof inputs);
    displayMenu();
    char c = getchar();
    int i = 0;
    
    while(c != '\n') {
      inputs[i] = c;
      c = getchar();
      i++;
    }
    
    switchStatements(inputs, &image, height, width, readCheck, changed);

  } while(inputs[0] != 'q' || inputs[1] != '\0');
  
  free(image);
  free(height);
  free(width);
  free(readCheck);
  free(changed);
  
  return 0;
}
