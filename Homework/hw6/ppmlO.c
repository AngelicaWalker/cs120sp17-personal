/*
    Homework 6, 600.120 Spring 2017

    Angelica Walker
    Section 02 - Kazhdan
    04/09/17
    awalke57
    awalke57@jhu.edu

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ppmlO.h"

int check(FILE *fp, int* height, int* width) {

  if(fgetc(fp) != 'P') {
    return 0;
  }
  if(fgetc(fp) != '6') {
    return 0;
  }
  
  fgetc(fp);

  char c = fgetc(fp);

  while(c  == '#') {
    
    while(c != '\n') {
      
      c = fgetc(fp);

    }

    c = fgetc(fp);

  }
  
  int max;

  ungetc(c,fp);
  
  fscanf(fp, "%d%d%d", width, height, &max);
  
  if(max != 255) {

    return 0;

  }

  return 1;
}

int read(char file[], Image ** image, int * height, int * width) {

  FILE *fp = fopen(file, "rb");

  if(!fp){
    printf("Error: there is no current image\n");
    return 0;
  }

  
  int checkNum = check(fp, height, width);
  
  if(!checkNum) {
      fclose(fp);
      printf("Error: there is no current image\n");
      return 0;
  }
  
  else {
    * image = (Image *) malloc(sizeof(Image) * (* height) * (* width));
    fread(* image, sizeof(Image),(* height) * (* width), fp);
    fclose(fp);
    return 1;
  }

  return 0;
}

int write(char file[], Image ** image, int * height, int * width) {

  char header[100];
  int size;
  
  size = sprintf(header, "P6\n%d %d\n255  \n", * width, * height);
  
  FILE* fp = fopen(file, "wb");
  
  if (!fp) {
    printf("Error: there is no image\n");
    return 0;
  }
  
  fwrite(header, sizeof(char), size, fp);
  
  fwrite(* image,sizeof(Image), (* height) * (* width), fp);
  
  fclose(fp);
  
  return 0;
}
