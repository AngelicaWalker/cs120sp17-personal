/*
    Homework 6, 600.120 Spring 2017

    Angelica Walker
    Section 02 - Kazhdan
    04/09/17
    awalke57
    awalke57@jhu.edu

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "menuUtil.h"
#include "imageManip.h"

int displayMenu() {
  printf(
    "Main menu:\n"
    "\n"
    "	r <filename> - read image from <filename>\n"
    "	w <filename> - write image to <filename>\n"
    "	c <x1> <y1> <x2> <y2> - crop image to the box with the given corners\n"
    "	i - invert intensities\n"
    "  	s - swap color channels\n"
    "	g - convert to grayscale\n"
    "	b <amt> - change brightness (up or down) by the given amount\n"
    "	q - quit\n"
    "Enter choice: ");
  return 0;
}

int switchStatements(char inputs[], Image ** image, int * height, int * width, int * readCheck, int * changed) {

  int numWords = wordCount(inputs);
  char first[2];
  char second[200];

  first[0] = inputs[0];
  first[1] = inputs[1];

  /*
  if (first[1] != ' ') {
    printf("Error: Unknown command\n");
    return 0;
  }
  */
  
  if (numWords == 3 || numWords == 4 || numWords > 5) {
    printf("Error: Unknown command\n");
    return 0;
  }

  switch (numWords) {
  case 1:

    if (first[0] == 'i' && first[1] == '\0') {
      printf("Inverting intensity...\n");

      if (* readCheck != 1) {
	printf("Error: there is no current image\n");
	return 0;
      }
      
      invert(image, height, width);
      * changed = 1;
    }

    else if (first[0] == 's' && first[1] == '\0') {
      printf("Swapping color channels...\n");

      if (* readCheck != 1) {
	printf("Error: there is no current image\n");
	return 0;
      }

      swap(image, height, width);
      * changed = 1;
    }

    else if (first[0] == 'g' && first[1] == '\0') {
      printf("Converting to grayscale...\n");

      if (* readCheck != 1) {
	printf("Error: there is no current image\n");
	return 0;
      }
      grayscale(image, height, width);
      * changed = 1;
    }
    
    else if (first[0] == 'q' && first[1] == '\0') {
      printf("Goodbye!\n");
      return 1;
    }
    
    else {
      printf("Error: Unknown Command\n");
    }

    break;

  case 2:

    if (first[0] == 'r' && first[1] == ' ') {
      sscanf(inputs, "%s %s", first, second);
      printf("Reading from %s...\n" , second);
      read(second, image, height, width);
      * readCheck = 1;  
    }

    else if (first[0] == 'w' && first[1] == ' ') {
      sscanf(inputs, "%s %s", first, second);
      printf("Writing to %s...\n" , second);

      if (* readCheck != 1) {
        printf("Error: there is no current image\n");
        return 0;
      }
      write(second, image, height, width);
    }
    
    else if (first[0] == 'b' && first[1] == ' ') {

      double brightening;
      sscanf(inputs, "%s %lf", first, &brightening);

      printf("Adjusting brightness by %lf...\n", brightening);

      if (* readCheck != 1) {
	printf("Error: there is no current image\n");
	return 0;
      }

      brightness(image, height, width,  brightening);
      * changed = 1;

    }

    else {
      printf("Error: Unknown command\n");
    }
    break;

  case 5:

    if (first[0] == 'c' && first[1] == ' ') {

      int x1;
      int y1;
      int x2;
      int y2;

      sscanf(inputs, "%s %d %d %d %d", first, &x1, &y1, &x2, &y2);

      printf("Cropping region from (%d, %d) to (%d, %d)...\n", x1, y1, x2, y2);

      if (* readCheck != 1) {
	printf("Error: there is no current image\n");
	return 0;
      }
      
      crop(image, height, width, x1, y1, x2, y2);
      * changed = 1;
    }

    break;
  }

  return 0;
}

int wordCount(char inputs[]) {

  int numWords = 0;
  
  for (unsigned int i = 0; i < strlen(inputs); i++) {

    if(inputs[i] == ' ') {

      numWords++;
      
      if (i < strlen(inputs) - 1 && inputs[i+1] == ' ') {
        numWords--;
      }
    }
  }
  
  numWords++;
  
  return numWords;
}
