/*
    Homework 6, 600.120 Spring 2017

    Angelica Walker
    Section 02 - Kazhdan
    04/09/17
    awalke57
    awalke57@jhu.edu

*/

#include "ppmlO.h"

int displayMenu();
int switchStatements(char inputs[], Image ** image, int * height, int * width, int * readCheck, int * changed);
int wordCount(char inputs[]);
