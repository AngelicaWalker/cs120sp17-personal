/*
    Homework 5, 600.120 Spring 2017

    Angelica Walker
    Section 02 - Kazhdan
    04/02/17
    awalke57
    awalke57@jhu.edu

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "prompts.h"
#include "LinkedList.h"
#include "functions.h"


int main(int argc, char *argv[]) {


  FILE* fp = fopen(argv[1], "r");

  if (argc == 1) {
    return 1;
  }

  if (!fp) {
    printf("Error opening course catalog");
    return 1;
  }
  
  char c;
  int lines = 0;

  for (c = getc(fp); c != EOF; c = getc(fp)) {
    if (c == '\n') {
      lines++;
    }
  }
  
  struct Course * catalog = (struct Course *) malloc(sizeof(struct Course) * lines);
  struct node * transcript = (struct node *) malloc(sizeof(struct node));

  rewind(fp);
  
  for (int i = 0; i < lines; i++) {

    fscanf(fp, "%[^.].%4d%*c%4d %d%*c%d %[^\n]\n", catalog[i].division, &catalog[i].department, &catalog[i].number, &catalog[i].credits, &catalog[i].halfCredit, catalog[i].title);
    
  }
  

  fclose(fp);

  char choice;
  
  do {
  menu_prompt();

  while ((choice = getchar()) == '\n');

  switch (choice) {
  case '1':

    for (int j = 0; j < lines; j++) {
      printf("%s.%d.%d %d.%d %s\n", catalog[j].division, catalog[j].department, catalog[j].number, catalog[j].credits, catalog[j].halfCredit, catalog[j].title);
    }
    printf("\n");

    break;
  case '2':
    course_prompt();
    dispInfo(lines, catalog);
    break;
  case '3':
    course_prompt();
    updateTitle(lines, catalog);
    break;
  case '4':
    course_prompt();
    updateCredits(lines, catalog);
    break;
  case '5':
    course_prompt();
    addCourse(lines, catalog, transcript);
    break;
  case '6':
    course_prompt();
    removeCourse(lines, catalog, transcript);
    break;
  case '7':
    printTranscript(transcript);
    break;
  case '8':
    course_prompt();
    searchPrint(lines, catalog, transcript);
    break;
  case '9':
    calculate(transcript);
    break;
  case 'q': case 'Q':
    return 1;
    break;
  default:
    invalid_input_msg();
    break;
  }
 
  } while (choice != 'q');

  free(catalog);
  free(transcript);
  return 0;

}
