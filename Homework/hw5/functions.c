/*
    Homework 5, 600.120 Spring 2017

    Angelica Walker
    Section 02 - Kazhdan
    04/02/17
    awalke57
    awalke57@jhu.edu

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "functions.h"
#include "prompts.h"

int validCourseInput(char text[]) {

  int len = strlen(text);

  if (len > 10) {
    invalid_input_msg();
    course_prompt();
    return 1;
  }

  char div[3];
  char dep[4];
  char num[4];

  div[0] = text[0];
  div[1] = text [1];
  div[2] = '\0';

  dep[0] = text[3];
  dep[1] = text[4];
  dep[2] = text[5];
  dep[3] = '\0';
  
  num[0] = text[7];
  num[1] = text[8];
  num[2] = text[9];
  num[3] = '\0';

  int depnum = atoi(dep);
  int numnum = atoi(num);

  if (strcmp(div, "ME") == 0) {
    return 0;
  }
  else if (strcmp(div, "BU") == 0) {
    return 0;
  }
  else if (strcmp(div, "ED") == 0) {
    return 0;
  }
  else if (strcmp(div, "EN") == 0) {
    return 0;
  }
  else if (strcmp(div, "AS") == 0) {
    return 0;
  }
  else if (strcmp(div, "PH") == 0) {
    return 0;
  }
  else if (strcmp(div, "PY") == 0) {
    return 0;
  }
  else if (strcmp(div, "SA") == 0) {
    return 0;
  }
  else if (strcmp(div, "me") == 0) {
    return 0;
  }
  else if (strcmp(div, "bu") == 0) {
    return 0;
  }
  else if (strcmp(div, "ed") == 0) {
    return 0;
  }
  else if (strcmp(div, "en") == 0) {
    return 0;
  }
  else if (strcmp(div, "as") == 0) {
    return 0;
  }
  else if (strcmp(div, "ph") == 0) {
    return 0;
  }
  else if (strcmp(div, "py") == 0) {
    return 0;
  }
  else if (strcmp(div, "sa") == 0) {
    return 0;
  }
  else {
    invalid_input_msg();
    course_prompt();
    return 1;
  }


  if (depnum > 999 || depnum < 0) {
    invalid_input_msg();
    course_prompt();
    return 1;
  }

  if (numnum > 999 || numnum < 0) {
    invalid_input_msg();
    course_prompt();
    return 1;
  }

  return 0;

}

int validSemesterInput(char text[]) {

  int len = strlen(text);

  if (len > 7) {
    invalid_input_msg();
    semester_prompt();
    return 1;
  }

  char yearstr[5];
  char sem;

  yearstr[0] = text[0];
  yearstr[1] = text[1];
  yearstr[2] = text[2];
  yearstr[3] = text[3];
  yearstr[4] = '\0';

  sem = text[5];

  int yearnum = atoi(yearstr); 

  if (sem == 'F') {
    return 0;
  }
  else if (sem == 'f') {
    return 0;
  }
  else if (sem == 'S') {
    return 0;
  }
  else if (sem == 's') {
    return 0;
  }
  else {
    invalid_input_msg();
    semester_prompt();
    return 1;
  }

  if (yearnum > 9999 || yearnum < 0) {
    invalid_input_msg();
    semester_prompt();
    return 1;
  }

  return 0;
}

int validGradeInput(char text[]) {

  int len = strlen(text);

  if (len > 3) {
    invalid_input_msg();
    grade_prompt();
    return 1;
  }

  char letterGrade;
  char gradeSign;

  letterGrade = text[0];

  gradeSign = text[1];

  if (letterGrade == 'A') {
    return 0;
  }
  else if (letterGrade == 'a') {
    return 0;
  }
  else if (letterGrade == 'B') {
    return 0;
  }
  else if (letterGrade == 'b') {
    return 0;
  }
  else if (letterGrade == 'C') {
    return 0;
  }
  else if (letterGrade == 'c') {
    return 0;
  }
  else if (letterGrade == 'D') {
    return 0;
  }
  else if (letterGrade == 'd') {
    return 0;
  }
  else if (letterGrade == 'F') {
    return 0;
  }
  else if (letterGrade == 'f') {
    return 0;
  }
  else {
    invalid_input_msg();
    grade_prompt();
    return 1;
  }

  if (gradeSign == '+') {
    return 0;
  }
  else if (gradeSign == '-') {
    return 0;
  }
  else if (gradeSign == '/') {
    return 0;
  }
  else {
    invalid_input_msg();
    grade_prompt();
    return 1;
  }

  return 0;
}
int dispInfo(int l, struct Course * catalog) {

  int check = 0;
  char input[1000];
  //int placeholder = 0;

  scanf("%s", input);
  check = validCourseInput(input);
  
  if (check == 1) {
    dispInfo(l, catalog);
  }

  if (check == 0) {

    char div[3];
    char dep[4];
    char num[4];

    div[0] = input[0];
    div[1] = input[1];
    div[2] = '\0';

    dep[0] = input[3];
    dep[1] = input[4];
    dep[2] = input[5];
    dep[3] = '\0';

    num[0] = input[7];
    num[1] = input[8];
    num[2] = input[9];
    num[3] = '\0';

    int depnum = atoi(dep);
    int numnum = atoi(num);

    int found = 0;
    int position = 0;
    
    for (int i = 0; i < l; i++) {

      int testing = 0;

      
      if (strcmp(catalog[i].division, div) == 0) {
        testing++;

      }
      
      if (catalog[i].department == depnum) {
	testing++;
      }

      if (catalog[i].number == numnum) {
        testing++;
      }


      if (testing == 3) {
        found = 1;
	position = i;
	break;
      }

    }

    if (found == 1) {
      
	printf("%s.%d.%d %d.%d %s\n", catalog[position].division, catalog[position].department, catalog[position].number, catalog[position].credits, catalog[position].halfCredit, catalog[position].title);

    }

    if (!found) {
      course_absent_msg();
      course_prompt();
      dispInfo(l, catalog);
    }
    
  }

  
  return 0;

}

int updateTitle(int l, struct Course * catalog) {
  
  char input[1000];
  char newTitle[32];
  int check;

  scanf("%s", input);
  check = validCourseInput(input);

  if (check == 1) {
    updateTitle(l, catalog);
  }
  
  if (check == 0) {

    new_title_prompt();
    scanf("%s", newTitle);
    
    char div[3];
    char dep[4];
    char num[4];

    div[0] = input[0];
    div[1] = input[1];
    div[2] = '\0';

    dep[0] = input[3];
    dep[1] = input[4];
    dep[2] = input[5];
    dep[3] = '\0';

    num[0] = input[7];
    num[1] = input[8];
    num[2] = input[9];
    num[3] = '\0';

    int depnum = atoi(dep);
    int numnum = atoi(num);

    int found = 0;
    int position = 0;

    for (int i = 0; i < l; i++) {

      int testing = 0;


      if (strcmp(catalog[i].division, div) == 0) {
        testing++;

      }

      if (catalog[i].department == depnum) {
        testing++;
      }

      if (catalog[i].number == numnum) {
        testing++;
      }

      
      if (testing == 3) {
        found = 1;
        position = i;
        break;
      }

    }

    if (found == 1) {

      strcpy(catalog[position].title, newTitle);
      course_updated_msg();

    }
 
    if (!found) {
      course_absent_msg();
      course_prompt();
      updateTitle(l, catalog);
    }


  }
  

  return 0;
}

int updateCredits(int l, struct Course * catalog) {

  char input[1000];
  int newCredit;
  int newCreditHalf;
  int check;

  scanf("%s", input);
  check = validCourseInput(input);

  if (check == 1) {
    updateCredits(l, catalog);
  }

  if (check == 0) {

    new_credit_prompt();
    scanf("%d%*c%d", &newCredit, &newCreditHalf);

    char div[3];
    char dep[4];
    char num[4];

    div[0] = input[0];
    div[1] = input[1];
    div[2] = '\0';

    dep[0] = input[3];
    dep[1] = input[4];
    dep[2] = input[5];
    dep[3] = '\0';

    num[0] = input[7];
    num[1] = input[8];
    num[2] = input[9];
    num[3] = '\0';

    int depnum = atoi(dep);
    int numnum = atoi(num);

    int found = 0;
    int position = 0;
    
    for (int i = 0; i < l; i++) {

      int testing = 0;


      if (strcmp(catalog[i].division, div) == 0) {
        testing++;

      }

      if (catalog[i].department == depnum) {
        testing++;
      }

      if (catalog[i].number == numnum) {
        testing++;
      }


      if (testing == 3) {
        found = 1;
        position = i;
        break;
      }

    }

    if (found == 1) {

      catalog[position].credits = newCredit;
      catalog[position].halfCredit = newCreditHalf;

      course_updated_msg();
    }

    if (!found) {
      course_absent_msg();
      course_prompt();
      updateCredits(l, catalog);
    }


  }
  return 0;
}


int addCourse(int l, struct Course * catalog, struct node * transcript) {

  char input[1000];
  int check;
  char inputSemester[1000];
  int checkSem;
  char inputGrade[1000];
  int checkGrade;

  scanf("%s", input);
  check = validCourseInput(input);

  if (check == 1) {
    addCourse(l, catalog, transcript);
  }

  if (check == 0) {

    char div[3];
    char dep[4];
    char num[4];

    div[0] = input[0];
    div[1] = input[1];
    div[2] = '\0';

    dep[0] = input[3];
    dep[1] = input[4];
    dep[2] = input[5];
    dep[3] = '\0';

    num[0] = input[7];
    num[1] = input[8];
    num[2] = input[9];
    num[3] = '\0';

    int depnum = atoi(dep);
    int numnum = atoi(num);

    int found = 0;
    int position = 0;

    for (int i = 0; i < l; i++) {

      int testing = 0;


      if (strcmp(catalog[i].division, div) == 0) {
        testing++;

      }

      if (catalog[i].department == depnum) {
        testing++;
      }

      if (catalog[i].number == numnum) {
        testing++;
      }


      if (testing == 3) {
        found = 1;
        position = i;
        break;
      }

    }

    if (found == 1) {

      semester_prompt();
      scanf("%s", inputSemester);

      checkSem = validSemesterInput(inputSemester);

      if (checkSem == 1) {
	addCourse(l, catalog, transcript);
      }
      else {

	grade_prompt();
	scanf("%s", inputGrade);

	checkGrade = validGradeInput(inputGrade);

	if (checkGrade == 1) {
	  addCourse(l, catalog, transcript);
	}
	else {


	  struct node * temp = (struct node *)malloc(sizeof(struct node));

	  char yearstr[5];
	  char sem;

	  yearstr[0] = inputSemester[0];
	  yearstr[1] = inputSemester[1];
	  yearstr[2] = inputSemester[2];
	  yearstr[3] = inputSemester[3];
	  yearstr[4] = '\0';

	  sem = inputSemester[5];

	  int yearnum = atoi(yearstr);



	  temp->department = catalog[position].department;
	  temp->number = catalog[position].number;
	  temp->credits = catalog[position].credits;
	  temp->halfCredit = catalog[position].halfCredit;
	  temp->year = yearnum;
	  temp->semester = sem;
	  temp->next = NULL;
	  strcpy(temp->division, catalog[position].division);
	  strcpy(temp->title, catalog[position].title);
	  strcpy(temp->grade, inputGrade);

	  if (head == NULL) {
	    head = temp;
	    head->next = NULL;
	  }
	  else {
	    temp->next = head;
	    head=temp;
	  }
	  transcript_updated_msg();
	  //free(temp);
	}
      }
      
    }

    if (!found) {
      course_absent_msg();
      course_prompt();
      addCourse(l, catalog, transcript);
    }


  }

  //  free(temp);
  return 0;

}

int removeCourse(int l, struct Course * catalog, struct node * transcript) {

  char input[1000];
  int check;
  int checkTrans;


  scanf("%s", input);
  check = validCourseInput(input);

  if (check == 1) {
    removeCourse(l, catalog, transcript);
  }

  if (check == 0) {

    char div[3];
    char dep[4];
    char num[4];

    div[0] = input[0];
    div[1] = input[1];
    div[2] = '\0';

    dep[0] = input[3];
    dep[1] = input[4];
    dep[2] = input[5];
    dep[3] = '\0';

    num[0] = input[7];
    num[1] = input[8];
    num[2] = input[9];
    num[3] = '\0';

    int depnum = atoi(dep);
    int numnum = atoi(num);

    int found = 0;
    //int position = 0;

    for (int i = 0; i < l; i++) {

      int testing = 0;


      if (strcmp(catalog[i].division, div) == 0) {
        testing++;

      }

      if (catalog[i].department == depnum) {
        testing++;
      }

      if (catalog[i].number == numnum) {
        testing++;
      }


      if (testing == 3) {
        found = 1;
	//  position = i;
        break;
      }

    }
    
    if (found == 1) {
      checkTrans = search(div, depnum, numnum);

      if (checkTrans == 2) {
	return 0;
      }
      if (checkTrans == 1) {
        course_not_taken_msg();
      }
      else {

	struct node *temp = (struct node *)malloc(sizeof(struct node));
	struct node *prev = (struct node *)malloc(sizeof(struct node));;
	
	temp = head;
	while (temp != NULL) {
	  if(temp == head) {
	    head = temp->next;
	    free(temp);
	    transcript_updated_msg();
	    return 0;
	  }
	  else {
	    prev->next = temp->next;
	    free(temp);
	    transcript_updated_msg();
	    return 0;
	  }
	}
      }
    }

    if (!found) {
      course_absent_msg();
      course_prompt();
      removeCourse(l, catalog, transcript);
      }
    }

  //  free(temp);
  //free(prev);
  return 0;

}



int printTranscript(struct node * transcript) {

  transcript = head;

  if(transcript == NULL) {
    empty_transcript_msg();
  }

  while(transcript != NULL) {
    printf("%d.%c %s %s.%d.%d %d.%d %s\n", transcript->year, transcript->semester, transcript->grade, transcript->division, transcript->department, transcript->number, transcript->credits, transcript->halfCredit, transcript->title);
    transcript = transcript->next;
  }

  return 0;
}

int search(char * div, int depnum, int numnum) {

  struct node * temp = (struct node *)malloc(sizeof(struct node));
  int i = 0;

  if (head == NULL) {
    return 2;
  }
  else {
    for (temp = head; temp != NULL; temp = temp->next) {
      i++;
      int testing = 0;
      if (strcmp(temp->division, div) == 0) {
	testing++;
      }
      if (temp->department == depnum) {
	testing++;
      }
      if (temp->number == numnum) {
	testing++;
      }
      if (testing == 3) {
	return 0;
      }
      else {
	return 1;
      }
    }
  }

  free(temp);

  return 0;
}

int searchPrint(int l, struct Course * catalog, struct node * transcript) {

  char input[1000];
  int check;
  int checkTrans;

  scanf("%s", input);
  check = validCourseInput(input);

  if (check == 1) {
    searchPrint(l, catalog, transcript);
  }

  if (check == 0) {

    char div[3];
    char dep[4];
    char num[4];

    div[0] = input[0];
    div[1] = input[1];
    div[2] = '\0';

    dep[0] = input[3];
    dep[1] = input[4];
    dep[2] = input[5];
    dep[3] = '\0';

    num[0] = input[7];
    num[1] = input[8];
    num[2] = input[9];
    num[3] = '\0';

    int depnum = atoi(dep);
    int numnum = atoi(num);

    int found = 0;

    for (int i = 0; i < l; i++) {

      int testing = 0;


      if (strcmp(catalog[i].division, div) == 0) {
        testing++;

      }

      if (catalog[i].department == depnum) {
        testing++;
      }

      if (catalog[i].number == numnum) {
        testing++;
      }


      if (testing == 3) {
        found = 1;
        break;
      }

    }

    if (found == 1) {

      checkTrans = search(div, depnum, numnum);

      if (checkTrans == 2) {
	return 0;
      }
      if (checkTrans == 1) {
	course_not_taken_msg();
      }
      else {
	struct node * temp = (struct node *)malloc(sizeof(struct node));
	int i = 0;

	for (temp = head; temp != NULL; temp = temp->next) {
	  i++;
	  int testing = 0;

	  if (strcmp(temp->division, div) == 0) {
	    testing++;
	  }
	  if (temp->department == depnum) {
	    testing++;
	  }
	  if (temp->number == numnum) {
	    testing++;
	  }

	  if (testing == 3) {
	    printf("%d.%c %s %s.%d.%d %d.%d %s\n", transcript->year, transcript->semester, transcript->grade, transcript->division, transcript->department, transcript->number, transcript->credits, transcript->halfCredit, transcript->title);
    transcript = transcript->next;
	  }
	  free(temp);
	}
      }
    }
    
    if (!found) {
      course_absent_msg();
      course_prompt();
      removeCourse(l, catalog, transcript);
      }
  }

  return 0;
}

int calculate(struct node * transcript) {

  int GPA;
  
  if (head == NULL) {
    gpa_msg(0);
    return 0;
  }

  while (transcript != NULL) {

    char gradestr[3];
    char gradeLetter;
    int gradeNum = 0;

    strcpy(gradestr, transcript->grade);

    gradeLetter = gradestr[0];

    switch (gradeLetter) {
    case 'A':
      gradeNum = 0;
      break;
    case 'B':
      gradeNum = 1;
      break;
    case 'C':
      gradeNum = 2;
      break;
    case 'D':
      gradeNum = 3;
      break;
    case 'F':
      gradeNum = 4;
      break;
    case 'I':
      gradeNum = 5;
      break;
    case 'S':
      gradeNum = 6;
      break;
    case 'U':
      gradeNum = 7;
      break;
    }

    GPA = GPA + gradeNum;

    
    transcript = transcript->next;
  }

  return 0;
}
