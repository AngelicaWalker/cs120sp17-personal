/*
    Homework 5, 600.120 Spring 2017

    Angelica Walker
    Section 02 - Kazhdan
    04/02/17
    awalke57
    awalke57@jhu.edu
*/
struct Course {

  char division[3];
  int department;
  int number;
  int credits;
  int halfCredit;
  char title[33];

} course;

struct node {

  char division[3];
  int department;
  int number;
  int credits;
  int halfCredit;
  char title[33];
  int year;
  char semester;
  char grade[3];
  struct node *next;

} *head;

//typedef struct node snode;
//snode *newnode, *ptr, *prev, *temp;
//snode *first = NULL, *last = NULL;

int validCourseInput(char text[]);
int validSemesterInput(char text[]);
int validGradeInput(char text[]);
int dispInfo(int l, struct Course * catalog);
int updateTitle(int l, struct Course * catalog);
int updateCredits(int l, struct Course * catalog);
int addCourse(int l, struct Course * catalog, struct node * transcript);
int removeCourse(int l, struct Course * catalog, struct node * transcript);
int printTranscript(struct node * transcript);
int search(char * div, int depnum, int numnum);
int searchPrint(int l, struct Course * catalog, struct node * transcript);
int calculate(struct node * transcript);
